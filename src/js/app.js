import 'jquery';
var css = require('../scss/app.scss');

// A $( document ).ready() block.
$( document ).ready(function() {
    // the toggle menu
    $('.toggle-menu').on("click", function(){
    	if( $(this).hasClass('opened-nav') ) {
    		//$('#wrapper').find('.navbar-static-side').css("display", "none");
    		$('#wrapper').find('.navbar-static-side').animate({ marginLeft: '-160px'}, 300);
    		//$('#wrapper').find('.page-wrapper').css('margin-left', '0');
    		$('#wrapper').find('.page-wrapper').animate({ marginLeft: '0px'}, 300);
    		$(this).removeClass("opened-nav").addClass("hidden-nav");
    	} else {
    		//$('#wrapper').find('.navbar-static-side').css("display", "block");
    		$('#wrapper').find('.navbar-static-side').animate({ marginLeft: '0px'}, 300);
    		//$('#wrapper').find('.page-wrapper').css('margin-left', '160px');
    		$('#wrapper').find('.page-wrapper').animate({ marginLeft: '+160px'}, 300);
    		$(this).removeClass("hidden-nav").addClass("opened-nav");
    	}
    });

    // dynamic height of the sidebar
    var contentHeight = $('.page-wrapper').height();
    $('.navbar-static-side').css("height", contentHeight+'px');

    // show hide notifications in main navigation
    $('.nav-notifications-item').on("click", function(){
        // not exact behaviour we are after - the dismiss should be if clicking outside the container
        // -- || -- the transition should be smooth, with a fade in / drop animation
        $(this).find('.notifications-container').toggle();
    });

    //show search in main navigation
    $('.main-nav-item .icon-magnifier-glass').on("click", function(){
        // not exact behaviour we are after - the transition should be smooth
        $(this).parent().find('.search-holder').show();
        $(this).parents().find('.icon-magnifier-glass').first().hide();
        $(this).parents().find('.nav-notifications-item').hide();
        $(this).parents().find('.vertical-bar').first().hide();
    });

    //hide search in main navigation
    $('.dismiss-search').on("click", function(){
        $(this).parent().hide();
        $(this).parents().find('.icon-magnifier-glass').first().show();
        $(this).parents().find('.nav-notifications-item').show();
        $(this).parents().find('.vertical-bar').first().show();
    });

    // show hide user-logged-container in the main navigation
    $('.user-logged-item').on("click", function(){
        // not exact behaviour we are after - the dismiss should be if clicking outside the container
        // -- || -- the transition should be smooth, with a fade in / drop animation
        $(this).find('.user-logged-container').toggle();
    });

    // keeping active the number field on hover over plus minus btns
    $('.number-field-btn.minus-btn').on("mouseenter", function(){
        $(this).parent().find('.number-control').addClass("active");
    });

    $('.number-field-btn.minus-btn').on("mouseleave", function(){
        $(this).parent().find('.number-control').removeClass("active");
    });

    $('.number-field-btn.plus-btn').on("mouseenter", function(){
        $(this).parent().find('.number-control').addClass("active");
    });

    $('.number-field-btn.plus-btn').on("mouseleave", function(){
        $(this).parent().find('.number-control').removeClass("active");
    });

});

// google map exemple 1 - simple map
(function(window, google){



  // The action happens in self invoking function

  // Configure map options

  // ---------------------



  var options = {

    center: {

      lat: 25.686614,

      lng: -100.316113

      //to get the latitude and longitude use: http://www.latlong.net/

    },

    zoom: 12,

    styles: [
        {
            "featureType": "administrative.country",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        }
    ], // snazzymaps.com

    zoomControl: true,

    mapTypeControl: false,

    scaleControl: false,

    streetViewControl: false,

    rotateControl: false,

    fullscreenControl: false,

    scrollwheel: false,

    draggable: true,

    zoomControlOptions: {

      position: google.maps.ControlPosition.TOP_RIGHT

    },

    mapTypeControlOptions: {

       position: google.maps.ControlPosition.TOP_CENTER

    }

  },
  

  // A div with the id #map was created before in the HTML document

  // ---------------------------------------------------------------------

  element = document.getElementById('map'),
  

  // Placing the map in the selected div above

  // -----------------------------------------

  map = new google.maps.Map(element, options);



}(window, google));


// google map example 2 - bottom ribbon
(function(window, google){



  // The action happens in self invoking function

  // Configure map options

  // ---------------------



  var options = {

    center: {

      lat: 25.686614,

      lng: -100.316113

      //to get the latitude and longitude use: http://www.latlong.net/

    },

    zoom: 12,

    styles: [
        {
            "featureType": "administrative.country",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        }
    ], // snazzymaps.com

    zoomControl: true,

    mapTypeControl: false,

    scaleControl: false,

    streetViewControl: false,

    rotateControl: false,

    fullscreenControl: false,

    scrollwheel: false,

    draggable: true,

    zoomControlOptions: {

      position: google.maps.ControlPosition.TOP_RIGHT

    },

    mapTypeControlOptions: {

       position: google.maps.ControlPosition.TOP_CENTER

    }

  },
  

  // A div with the id #map was created before in the HTML document

  // ---------------------------------------------------------------------

  element = document.getElementById('map2'),
  

  // Placing the map in the selected div above

  // -----------------------------------------

  map = new google.maps.Map(element, options);



}(window, google));


// google map example 3 - side panel
(function(window, google){



  // The action happens in self invoking function

  // Configure map options

  // ---------------------



  var options = {

    center: {

      lat: 25.686614,

      lng: -100.316113

      //to get the latitude and longitude use: http://www.latlong.net/

    },

    zoom: 12,

    styles: [
        {
            "featureType": "administrative.country",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        }
    ], // snazzymaps.com

    zoomControl: true,

    mapTypeControl: false,

    scaleControl: false,

    streetViewControl: false,

    rotateControl: false,

    fullscreenControl: false,

    scrollwheel: false,

    draggable: true,

    zoomControlOptions: {

      position: google.maps.ControlPosition.TOP_RIGHT

    },

    mapTypeControlOptions: {

       position: google.maps.ControlPosition.TOP_CENTER

    }

  },
  

  // A div with the id #map was created before in the HTML document

  // ---------------------------------------------------------------------

  element = document.getElementById('map3'),
  

  // Placing the map in the selected div above

  // -----------------------------------------

  map = new google.maps.Map(element, options);



}(window, google));

