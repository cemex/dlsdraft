var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');
var path = require('path');

module.exports = {
	entry: {
		app: './src/js/app.js'
	},
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: './js/[name].bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						'css-loader', 
						'postcss-loader', 
						'sass-loader'
					],
					publicPath: './dist'
				})
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				use: [
					'file-loader?name=[name].[ext]&outputPath=images/&publicPath=../',
					'image-webpack-loader'
				]
			}
		]
	},
	devServer: {
		contentBase: path.join(__dirname, "dist"),
		port: 9000,
		open: true,
		stats: "errors-only"
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery"
		}),
		new ExtractTextPlugin({
			filename: "./css/[name].css",
			allChunks: true
		}),
		new HtmlWebpackPlugin({
			title:'Start',
			template: './src/index.html'
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Grid System',
			filename: 'grid.html',
			template: './src/grid.html' // Load my template
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Typography',
			filename: 'typography.html',
			template: './src/typography.html' // Load my template
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Colors',
			filename: 'colors.html',
			template: './src/colors.html' // Load my template
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Buttons',
			filename: 'buttons.html',
			template: './src/buttons.html' // Load my template
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Icons',
			filename: 'icons.html',
			template: './src/icons.html' // Load my template
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Navigation',
			filename: 'navigation.html',
			template: './src/navigation.html' // Load my template
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Tables',
			filename: 'tables.html',
			template: './src/tables.html' // Load my template
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Maps',
			filename: 'maps.html',
			template: './src/maps.html' // Load my template
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Forms',
			filename: 'forms.html',
			template: './src/forms.html' // Load my template
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Alerts',
			filename: 'alerts.html',
			template: './src/alerts.html' // Load my template
		}),
		new HtmlWebpackPlugin({
			title: 'CEMEX Modals',
			filename: 'modals.html',
			template: './src/modals.html' // Load my template
		}),
	]
}